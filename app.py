from random import randint

import kivy
kivy.require('1.11.0')

from kivy.app import App
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.graphics import Rectangle
from kivy.properties import ListProperty, NumericProperty, ObjectProperty
from kivy.uix.widget import Widget


class SnakeGame(Widget):

    snake = ObjectProperty(None)
    food = ObjectProperty(None)

    start_speed = NumericProperty(1)

    score = NumericProperty(0)

    rows = 20
    cols = 20

    cell_size = NumericProperty(0)

    def __init__(self):
        super().__init__()

        Window.size = (800, 800)
        self.cell_size = (800 - 6) / self.rows

        self.timer = Clock.schedule_interval(self.update, 1)

    def start(self):
        pass

    def reset(self):
        pass

    def spawn_food(self):
        """
        Randomly spawn food.
        """

        x = 3 + self.cell_size * randint(0, self.rows - 1)
        y = 3 + self.cell_size * randint(0, self.cols - 1)
        print(x, y)

        # TODO - Check snake's body position
        while (x, y) in ():
            x = 3 + self.cell_size * randint(0, self.rows - 1)
            y = 3 + self.cell_size * randint(0, self.cols - 1)

        self.food.spawn(x, y)

    def update(self, dt):
        self.spawn_food()


class Snake(Widget):

    head = ObjectProperty(None)
    tail = ObjectProperty(None)

    def move(self):
        pass


class SnakeHead(Widget):

    pass


class SnakeTail(Widget):

    tail_blocks = ListProperty()

    def reset(self):
        tail_blocks = []

    def add_block(self):
        with self.canvas.before:
            Color(1, 1, 1, 1)
            # TODO - Add correct position
            block = Rectangle(pos=(10, 10), size=(self.width, self.height))
            tail_blocks.append(block)


class Food(Widget):

    # Time left before the food expires
    duration = NumericProperty(10)

    width = NumericProperty(5)
    height = NumericProperty(5)

    def spawn(self, x, y):
        self.x = x
        self.y = y

    def remove(self):
        pass


class SnakeApp(App):

    def build(self):
        return SnakeGame()


if __name__ == '__main__':
    SnakeApp().run()
